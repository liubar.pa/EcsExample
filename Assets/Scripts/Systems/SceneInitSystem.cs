using Leopotam.EcsLite;
using UnityEngine;
using UnityEditor;

namespace Client {
    sealed class SceneInitSystem : IEcsInitSystem 
    {
        public void Init(IEcsSystems systems) 
        {
            var world = systems.GetWorld();

            var circle_prefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Circle.prefab");
            var circle_game_object = GameObject.Instantiate(circle_prefab, new Vector3(0f, 0f, 0f), Quaternion.identity);

            var circle_entity = world.NewEntity();

            var game_objects_components_pool = world.GetPool<GameObjectComponent>();
            game_objects_components_pool.Add(circle_entity) = new GameObjectComponent{_game_object = circle_game_object, 
                                                                                      _transform = circle_game_object.GetComponent<Transform>()};

            var coordinate_components_pool = world.GetPool<CoordinateComponent>();
            coordinate_components_pool.Add(circle_entity) = new CoordinateComponent{_coordinate = new Vector3(0f, 0f, 0f)};

            var moving_components_pool = world.GetPool<MovingComponent>();
            moving_components_pool.Add(circle_entity) = new MovingComponent{_direction = new Vector3(1, 0, 0), _speed = 0.1f};
        }
    }
}