using Leopotam.EcsLite;

namespace Client 
{
    sealed class MovingSystem : IEcsRunSystem 
    {
        public void Run(IEcsSystems systems) 
        {
            var world = systems.GetWorld();

            var entitys_to_move = world.Filter<MovingComponent>().Inc<CoordinateComponent>().End();

            var coordinate_components_pool = world.GetPool<CoordinateComponent>();
            var moving_components_pool = world.GetPool<MovingComponent>();

            foreach (var entity in entitys_to_move)
            {
                ref var coordinate_component = ref coordinate_components_pool.Get(entity);
                ref var moving_component = ref moving_components_pool.Get(entity);  

                coordinate_component._coordinate += moving_component._direction * moving_component._speed;   
            }
        }
    }
}