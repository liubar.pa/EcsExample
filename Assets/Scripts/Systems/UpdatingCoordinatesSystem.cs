using Leopotam.EcsLite;
using UnityEngine;

namespace Client 
{
    sealed class UpdatingCoordinatesSystem : IEcsRunSystem 
    {        
        public void Run(IEcsSystems systems) 
        {
            var world = systems.GetWorld();

            var entitys = world.Filter<CoordinateComponent>().Inc<GameObjectComponent>().End();

            var coordinate_components_pool = world.GetPool<CoordinateComponent>();
            var game_object_components_pool = world.GetPool<GameObjectComponent>();

            foreach (var entity in entitys)
            {
                ref var coordinate_component = ref coordinate_components_pool.Get(entity);
                ref var game_object_component = ref game_object_components_pool.Get(entity);
                
                game_object_component._transform.position = coordinate_component._coordinate;  
            }
        }
    }
}