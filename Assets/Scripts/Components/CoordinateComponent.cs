using UnityEngine;

namespace Client 
{
    struct CoordinateComponent 
    {
        public Vector3 _coordinate;
    }
}