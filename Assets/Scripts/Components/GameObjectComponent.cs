using UnityEngine;

namespace Client 
{
    struct GameObjectComponent 
    {
        public GameObject _game_object;
        public Transform _transform;
    }
}