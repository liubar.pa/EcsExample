using UnityEngine;

namespace Client 
{
    struct MovingComponent 
    {
        public Vector3 _direction;
        public float _speed;
    }
}